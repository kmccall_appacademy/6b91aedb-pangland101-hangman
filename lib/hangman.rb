class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  def take_turn
    guessed_letter = @guesser.guess(board)
    guessed_letter_indicies = @referee.check_guess(guessed_letter)
    update_board(guessed_letter_indicies, guessed_letter)
    @guesser.handle_response(guessed_letter, guessed_letter_indicies)
  end

  def update_board(indicies, letter)
    indicies.each { |i| @board[i] = letter }
  end

  def play
    setup
    until @board.count(nil) == 0
      take_turn
    end
    @guesser.display(board)
    puts("Guesser wins!")
  end
end

class HumanPlayer
  def initialize
    @word_bank = []
  end

  def pick_secret_word
    puts("Enter the length of your super secret word")
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts("#{letter} has been guessed.")
    puts("Indices where that letter is located (sep. with spaces):")
    gets.chomp.split.map { |i| i.to_i }
  end

  def register_secret_length(word_length)
    puts("Word length is #{word_length}")
  end

  def guess(board)
    display(board)
    puts("Pick a letter! Any letter!")
    guess = gets.chomp

    until valid_guess?(guess)
      display(board)
      guess = gets.chomp
    end

    guess
  end

  def handle_response
    # I literally don't know what this is for
  end

  def display(board)
    print("Word: ")
    board.each { |el| el.nil? ? print("_") : print(el) }
    puts
  end

  def valid_guess?(guess)
    unless ('a'..'z').cover?(guess.downcase)
      puts("That uh. That's not a letter. You doing okay buddy?")
      return false
    end

    if @word_bank.include?(guess)
      puts("You tried that already. Try some memory exercises.")
      puts("Guessed letters: #{@word_bank}")
      return false
    end

    @word_bank << guess
    true
  end

end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary.map(&:chomp)
    @eligible_letters = ('a'..'z').to_a
  end

  def self.load_dictionary
    File.readlines('dictionary.txt')
  end

  def register_secret_length(word_length)
    @word_length = word_length
    candidate_words
  end

  def guess(board)
    display(board)
    board.each { |letter| @eligible_letters.delete(letter) }

    guess = @eligible_letters.sort_by do |letter|
      @dictionary.join("").count(letter)
    end.last

    @eligible_letters.delete(guess)
    guess
  end

  def handle_response(letter, indices)
    @dictionary = @dictionary.select do |word|
      characters = word.chars
      characters.each_index.all? do |i|
        indices.include?(i) ? word[i] == letter : word[i] != letter
      end
    end

    raise("Ineligible word; game over") if @dictionary.empty?
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    word_chars = @secret_word.chars
    word_chars.each_index.select { |i| word_chars[i] == letter }
  end

  def candidate_words
    @dictionary = @dictionary.select do |word|
      word.length == @word_length
    end
  end

  def display(board)
    print("Word: ")
    board.each { |el| el.nil? ? print("_") : print(el) }
    puts
  end
end
